package ba.unsa.etf.rma.nedzad.spirala;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import static ba.unsa.etf.rma.nedzad.spirala.KategorijeAkt.baza;

/**
 * Created by Nedzad on 21.05.2018..
 */

public class FragmentOnline extends Fragment implements  DohvatiKnjige.IDohvatiKnjigeDone,DohvatiNajnovije.IDohvatiNajnovijeDone,MojResultReceiver.Receiver{
    Spinner sRezultat;
    ArrayList<Knjiga> knjigee;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_online,container,false);

        final EditText upit= v.findViewById(R.id.tekstUpit);
        final Spinner sKategorije=v.findViewById(R.id.sKategorije);
         sRezultat=v.findViewById(R.id.sRezultat);
        Button dPretraga=v.findViewById(R.id.dRun);
        Button dDodaj=v.findViewById(R.id.dAdd);
        Button dPovratak=v.findViewById(R.id.dPovratak);


        ArrayAdapter<String> zaKategorije= new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_dropdown_item,ListaKnjiga.kategorije);
        sKategorije.setAdapter(zaKategorije);

        knjigee= new ArrayList<Knjiga>();


        dPretraga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                knjigee.clear();
                String query=upit.getText().toString();
                if(query.contains("autor")){
                    String[] naziv=query.split("\\:");
                    String autor=naziv[1];
                    new DohvatiNajnovije((DohvatiNajnovije.IDohvatiNajnovijeDone)FragmentOnline.this).execute(autor);
                }
                else if(query.contains(";")){
                    String[] nazivi=query.split("\\;");
                    for(String naslov: nazivi){
                        new DohvatiKnjige((DohvatiKnjige.IDohvatiKnjigeDone)FragmentOnline.this).execute(naslov);
                    }
                }
                else if(query.contains("korisnik:")){
                    String[] naziv=query.split("\\:");
                    String korisnik=naziv[1];
                    Intent intent = new Intent(Intent.ACTION_SYNC,null,getActivity(),KnjigePoznanika.class);
                    MojResultReceiver mReciever=new MojResultReceiver(new Handler());
                    mReciever.setReceiver(FragmentOnline.this);
                    intent.putExtra("idKorisnika", korisnik);

                    intent.putExtra("receiver",mReciever);
                    getActivity().startService(intent);
                }
                else{
                    new DohvatiKnjige((DohvatiKnjige.IDohvatiKnjigeDone)FragmentOnline.this).execute(query);

                }
               }
        });

        dPovratak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();
            }
        });

        dDodaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String naziv=sRezultat.getSelectedItem().toString();



                Boolean novak=true;
                for(Knjiga k: ListaKnjiga.listak){
                    if(k.getNaziv().equals(naziv)){
                        novak=false;
                    }
                }

                if(novak) {
                    for (int i = 0; i < knjigee.size(); i++) {
                        if (knjigee.get(i).getNaziv().equals(naziv)) {
                            knjigee.get(i).setZa(sKategorije.getSelectedItem().toString());

                            ListaKnjiga.listak.add(knjigee.get(i));
                             baza.dodajKnjigu(knjigee.get(i)); // za obrisati
                            Toast.makeText(getActivity(), "Knjiga " + knjigee.get(i).getNaziv() + " dodana u kategoriju" + knjigee.get(i).Zanr(), Toast.LENGTH_LONG).show();
           /*                 for (Autor a : knjigee.get(i).getAutori()) {
                                Boolean ima = false;
                                for (Autor aa : ListaKnjiga.autoriSvi) {

                                    if (a.getImeiPrezime().equals(aa.getImeiPrezime())) {
                                        ima = true;
                                        Boolean nova = true;
                                        for (String id : aa.getKnjige()) {
                                            if (id.equals(knjigee.get(i).getId())) {
                                                nova = false;
                                                aa.dodajKnjigu(knjigee.get(i).getId());
                                                aa.setBrk();
                                            }


                                        }

                                    } else {


                                    }

                                }
                                if (!ima) {
                                    ListaKnjiga.autoriSvi.add(new Autor(a.getImeiPrezime(), knjigee.get(i).getId()));
                                    ListaKnjiga.autoriSvi.get(ListaKnjiga.autoriSvi.size() - 1).setBrk();
                                }
                            }
*/

                            break;
                        }
                    }

                }
            }
        });



        return v;
    }
    @Override
    public void onDohvatiDone(ArrayList<Knjiga> knjige) {
            ArrayList<String> nazivi=new ArrayList<String>();


        for(Knjiga k:knjige){
            knjigee.add(k);
        }

        for(int i=0;i<knjigee.size();i++){
                nazivi.add(knjigee.get(i).getNaziv());
            }
        ArrayAdapter<String> sAdapter=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_dropdown_item,nazivi);
        sRezultat.setAdapter(sAdapter);



    }

    @Override
    public void onNajnovijeDone(ArrayList<Knjiga> knjige) {
        ArrayList<String> nazivi=new ArrayList<String>();
        for(int i=0;i<knjige.size();i++){
            nazivi.add(knjige.get(i).getNaziv());
        }
        ArrayAdapter<String> sAdapter=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_dropdown_item,nazivi);
        sRezultat.setAdapter(sAdapter);
        knjigee=knjige;
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode){
            case 0:
                Toast.makeText(getActivity(),"Pocelo",Toast.LENGTH_LONG).show();
                break;
            case 1:
                ArrayList<Knjiga> knjige =resultData.getParcelableArrayList("listaKnjiga");



                ArrayList<String> nazivi=new ArrayList<String>();
                for(int i=0;i<knjige.size();i++){
                    nazivi.add(knjige.get(i).getNaziv());
                }

                ArrayAdapter<String> adapter=new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, nazivi);
                sRezultat.setAdapter(adapter);
                knjigee=knjige;
                break;
            case 2:
                String error=resultData.getString(Intent.EXTRA_TEXT);
                Toast.makeText(getActivity(),error,Toast.LENGTH_LONG).show();
                break;

        }
    }
}
