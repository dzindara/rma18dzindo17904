package ba.unsa.etf.rma.nedzad.spirala;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by Nedzad on 22.05.2018..
 */

public class KnjigePoznanika extends IntentService {

    public int STATUS_START=0;
    public int STATUS_FINISH=1;
    public int STATUS_ERROR=2;
    private ArrayList<String> nasl=new ArrayList<String>();

    private ArrayList<Knjiga> listaKnjiga=new ArrayList<Knjiga>();

    //konsturktor
    public KnjigePoznanika(){
        super(null);
    }
    public KnjigePoznanika(String string){
        super(string);
    }


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String idK=intent.getStringExtra("idKorisnika");

        Bundle b = intent.getExtras();
        ResultReceiver mojRR=(ResultReceiver) b.get("receiver");
        Bundle bundle=new Bundle();
        mojRR.send(STATUS_START,Bundle.EMPTY);
        String query=null;
        try{

            query= URLEncoder.encode(idK,"utf-8");
        }
        catch (UnsupportedEncodingException e){
            e.printStackTrace();
        }

        ArrayList<Integer> shelve = new ArrayList<Integer>();

        //police
        try{   String url1 = "https://www.googleapis.com/books/v1/users/"+query+"/bookshelves";
            URL url = new URL(url1);
            URLConnection connection = url.openConnection();
            HttpURLConnection httpURLConnection = (HttpURLConnection) connection;

            InputStream in = new BufferedInputStream(httpURLConnection.getInputStream());

            String rezultat = convertStreamToString(in);
            JSONObject jo = new JSONObject(rezultat);

            JSONArray itemsArray = jo.getJSONArray("items");
            int j=0;
            for(int i=0; i<itemsArray.length();i++){
                JSONObject bookshelf = itemsArray.getJSONObject(i);
                if(bookshelf.getInt("volumeCount")!=0){

                    int id=bookshelf.getInt("id");
                    shelve.add(id);
                }
            }
        }
        catch (MalformedURLException e) {
            bundle.putString("greska","greska");
            mojRR.send(STATUS_ERROR,bundle);
            e.printStackTrace();
        } catch (IOException e) {
            bundle.putString("greska","greska");
            mojRR.send(STATUS_ERROR,bundle);
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }



            //knjige
        for (Integer id1:shelve) {


        try
        {

            String url1 = "https://www.googleapis.com/books/v1/users/" +query+"/bookshelves/"+id1.toString()+"/volumes";
            URL url = new URL(url1);
            URLConnection connection = url.openConnection();
            HttpURLConnection httpURLConnection = (HttpURLConnection) connection;

            InputStream in = new BufferedInputStream(httpURLConnection.getInputStream());

            listaKnjiga.clear();

            String rezultat= convertStreamToString(in);

            JSONObject jo = new JSONObject(rezultat);
            JSONArray ja = jo.getJSONArray("items");

            for(int i=0;i<ja.length();i++){

                JSONObject knjiga=ja.getJSONObject(i);
                JSONObject volinfo=knjiga.getJSONObject("volumeInfo");
                String id=knjiga.getString("id");
                String title;
                try {
                    title = volinfo.getString("title");
                }catch (Exception e)
                {
                    e.printStackTrace();
                    title="nema naslova";
                }


                nasl.add(title);

                ArrayList<Autor> autori=new ArrayList<Autor>();

                try{
                    JSONArray JSONautori=volinfo.getJSONArray("authors");
                    for(int j=0;j<JSONautori.length();j++){
                        String iipA = JSONautori.getString(j);
                        autori.add(new Autor(iipA,id));
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                }

                String opis;

                try{
                    opis=volinfo.getString("description");

                }
                catch (Exception e){
                    opis="nema opisa";
                    e.printStackTrace();
                }

                String datum;

                try{
                    datum=volinfo.getString("publishedDate");

                }
                catch (Exception e){
                    datum="nema datuma";
                    e.printStackTrace();
                }

                URL slika;
                try {
                    JSONObject slike = volinfo.getJSONObject("imageLinks");
                    slika = new URL(slike.getString("thumbnail"));
                } catch (Exception e) {
                    slika = new URL("https://");
                    e.printStackTrace();
                }

                int brojstr;
                try{
                    brojstr =volinfo.getInt("pageCount");
                }
                catch (Exception e){
                    brojstr=0;
                    e.printStackTrace();
                }

                listaKnjiga.add(new Knjiga(id,title,autori,opis,datum,slika,brojstr));

            }


        }
        catch (MalformedURLException e) {
            bundle.putString("greska","Error");
            mojRR.send(STATUS_ERROR,bundle);
            e.printStackTrace();
        } catch (IOException e) {
            bundle.putString("greska","Error");
            mojRR.send(STATUS_ERROR,bundle);
            e.printStackTrace();
        } catch (JSONException e) {
            bundle.putString("greska","Error");
            mojRR.send(STATUS_ERROR,bundle);
            e.printStackTrace();
        }

        }

        bundle.putParcelableArrayList("listaKnjiga",listaKnjiga);
        mojRR.send(STATUS_FINISH,bundle);

    }

    private String convertStreamToString(InputStream is)
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try
        {
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally
        {
            try
            {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }
}
