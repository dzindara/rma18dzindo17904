package ba.unsa.etf.rma.nedzad.spirala;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;


/**
 * Created by Nedzad on 18.05.2018..
 */

public class DohvatiKnjige extends AsyncTask<String, Integer, Void> {

    public interface IDohvatiKnjigeDone {
        public void onDohvatiDone(ArrayList<Knjiga> knjige);
    }

    private ArrayList<Knjiga> knjige = new ArrayList<Knjiga>();
    private IDohvatiKnjigeDone iPozivatelj;

    public DohvatiKnjige(IDohvatiKnjigeDone iP) {
        iPozivatelj = iP;
    }


    @Override
    protected Void doInBackground(String... params) {
        String query = null;
        try {
            query = URLEncoder.encode(params[0], "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url1 = "https://www.googleapis.com/books/v1/volumes?q=intitle:" + query + "&maxResults=5";

        try {
            URL url = new URL(url1);
            URLConnection konekc = url.openConnection();
            HttpURLConnection httpURLConnection = (HttpURLConnection) konekc;

            InputStream in = new BufferedInputStream(httpURLConnection.getInputStream());


            String rezultat = convertStreamToString(in);

            JSONObject jo = new JSONObject(rezultat);
            JSONArray ja = jo.getJSONArray("items");

            for (int i = 0; i < ja.length(); i++) {

                JSONObject knjiga = ja.getJSONObject(i);
                JSONObject volinfo = knjiga.getJSONObject("volumeInfo");
                String id = knjiga.getString("id");
                String title;
                try {
                    title = volinfo.getString("title");
                }catch (Exception e)
                {
                    e.printStackTrace();
                    title="nema naslova";
                }


                ArrayList<Autor> autori = new ArrayList<Autor>();

                try {
                    JSONArray JSONautori = volinfo.getJSONArray("authors");
                    for (int j = 0; j < JSONautori.length(); j++) {
                        String iipA = JSONautori.getString(j);
                        autori.add(new Autor(iipA, id));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                String opis;

                try {
                    opis = volinfo.getString("description");

                } catch (Exception e) {
                    opis = "nema opisa";
                    e.printStackTrace();
                }

                String datum;

                try {
                    datum = volinfo.getString("publishedDate");

                } catch (Exception e) {
                    datum = "nema datuma";
                    e.printStackTrace();
                }


                URL slika;
                try {
                    JSONObject slike = volinfo.getJSONObject("imageLinks");
                    slika = new URL(slike.getString("thumbnail"));

                } catch (Exception e) {
                    slika = new URL("https://");
                }
                int brojstr;
                try {
                    brojstr = volinfo.getInt("pageCount");
                } catch (Exception e) {
                    brojstr = 0;
                }

                knjige.add(new Knjiga(id, title, autori, opis, datum, slika, brojstr));
            }


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        iPozivatelj.onDohvatiDone(knjige);
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }
}
