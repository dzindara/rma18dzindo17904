package ba.unsa.etf.rma.nedzad.spirala;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.BadParcelableException;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import static ba.unsa.etf.rma.nedzad.spirala.KategorijeAkt.baza;

/**
 * Created by Nedzad on 08.04.2018..
 */

public class DodavanjeKnjigeFragment extends Fragment {
        ImageView image;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.dodavanje_knjige_fragment,container,false);


        image =(ImageView) view.findViewById(R.id.naslovnaStr);

        final EditText imeAutora =view.findViewById(R.id.imeAutora);
        final EditText naslovKnjige =view.findViewById(R.id.nazivKnjige);
        Button dodajSliku = view.findViewById(R.id.dNadjiSliku);
        Button dodajKnjigu = view.findViewById(R.id.dUpisiKnjigu);
        Button povratak = view.findViewById(R.id.dPonisti);
        final Spinner zanr = view.findViewById(R.id.sKategorijaKnjige);

        ArrayList<String> kategorije=getArguments().getStringArrayList("kat1");
        ArrayAdapter<String> kadapter=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_dropdown_item,kategorije);
        zanr.setAdapter(kadapter);

        povratak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(KategorijeAkt.sili){   KategorijeAkt.knjiged.setVisibility(View.VISIBLE);}
                getFragmentManager().popBackStack();
            }
        });

        dodajKnjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(imeAutora.getText().toString().equals("") || naslovKnjige.getText().toString().equals("")){
                    Toast.makeText(getActivity(),"Popunite sve podatke",Toast.LENGTH_SHORT).show();
                }
                else {
                    image.buildDrawingCache();
                    Knjiga tmp = new Knjiga(imeAutora.getText().toString(), naslovKnjige.getText().toString(), zanr.getSelectedItem().toString(), Bitmap.createBitmap(image.getDrawingCache()));
                  //  ListaKnjiga.listak.add(tmp);
                    baza.dodajKnjigu(tmp);
//_____
                    Boolean ima=false;
                    for(Autor a:ListaKnjiga.autoriSvi){
                       ima=false;
                        if(imeAutora.getText().toString().equals(a.IA())){
                            ima=true;
                            a.setBrk();
                            break;
                        }
                    }
                    if(!ima){
                        ListaKnjiga.autoriSvi.add(new Autor(imeAutora.getText().toString(),1));
                    }
                    //___


                    imeAutora.setText("");naslovKnjige.setText("");image.setImageResource(R.mipmap.ic_launcher);
                }
            }
        });

        dodajSliku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setType("image/*");
                i.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(i,""),2);
            }
        });
        return view;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if(requestCode==2 && resultCode== Activity.RESULT_OK){
            Uri uri1=data.getData();
            image.setImageURI(uri1);
        }

    }
}
