package ba.unsa.etf.rma.nedzad.spirala;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class ListaKnjigaAkt extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_knjiga_akt);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String string = bundle.get("zanr").toString();
        final ListView listak = findViewById(R.id.listaKnjiga);
        final ArrayList<Knjiga> kategoricno= new ArrayList<Knjiga>();
        for(Knjiga k: ListaKnjiga.listak){
            if(k.Zanr().equals(string)){
                kategoricno.add(k);
            }
        }

        final AdapterKnjige adapterknjige = new AdapterKnjige(this,kategoricno);
        listak.setAdapter(adapterknjige);
        Button povratak = findViewById(R.id.dPovratak);

        povratak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        listak.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                view.setBackgroundResource(R.color.selektovanaBoja);
                for(Knjiga k : ListaKnjiga.listak){
                    if(kategoricno.get(i).NazivK().equals(k.NazivK())){
                        k.Selektuj();
                    }
                }
            }
        });
    }
}
