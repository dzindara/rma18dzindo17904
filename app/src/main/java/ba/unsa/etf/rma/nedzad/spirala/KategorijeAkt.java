package ba.unsa.etf.rma.nedzad.spirala;

import android.app.FragmentManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.stetho.Stetho;

import java.util.ArrayList;

public class KategorijeAkt extends AppCompatActivity {
    static Boolean sili;
    static FrameLayout knjiged;
    public  static BazaOpenHelper baza;
      @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_kategorije_akt);
          Stetho.initializeWithDefaults(this);
        baza = new BazaOpenHelper(getApplicationContext());

        if(ListaKnjiga.kategorije.size()==0){
            ListaKnjiga.kategorije=baza.dajKategorije();


        }

        sili = false;
       //   baza.dodajKategoriju("test2");

          FragmentManager fm = getFragmentManager();
         knjiged= (FrameLayout)findViewById(R.id.mjestoF2);
        if(knjiged!=null){
            sili=true;
            KnjigeFragment kf;
            kf=(KnjigeFragment)fm.findFragmentById(R.id.mjestoF2);
            if(kf==null){
                kf=new KnjigeFragment();
                fm.beginTransaction().replace(R.id.mjestoF2,kf).commit();
            }
        }

          ListeFragment lf = (ListeFragment)fm.findFragmentByTag("Tag");
          if(lf==null){
              lf=new ListeFragment();
              fm.beginTransaction().replace(R.id.mjestoF,lf,"Tag").commit();
          }
          else{
              fm.popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
          }
/*
if(!sili){

}else{

    ListeFragment lf = (ListeFragment)fm.findFragmentById(R.id.mjestoF);
    if(lf==null){
        lf=new ListeFragment();
        fm.beginTransaction().replace(R.id.mjestoF1,lf).commit();
    }
    else{
        fm.popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }
}*/

     /* r final ArrayList<Knjiga> ListaKnjiga;
        final TextView pretraga = findViewById(R.id.dPretraga);
        TextView dknjig = findViewById(R.id.dDodajKnjigu);
        final TextView dkat = findViewById(R.id.dDodajKategoriju);

        final EditText srch = findViewById(R.id.tekstPretraga);
        final ListView kat = findViewById(R.id.listaKategorija);

        dkat.setEnabled(false);

        final ArrayList<String> kategorije = new ArrayList<String>();
        final ArrayAdapter<String> akategorije = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,kategorije);
        kat.setAdapter(akategorije);

        kategorije.add("Komedija");
        kategorije.add("Sci-Fi");

        pretraga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                akategorije.getFilter().filter(srch.getText().toString(), new Filter.FilterListener() {
                    @Override
                    public void onFilterComplete(int i) {
                        if(i==0 && !srch.getText().toString().isEmpty())
                        {dkat.setEnabled(true);}
                        else{
                            dkat.setEnabled(false);
                        }
                    }
                });


            }
        });

        dkat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                akategorije.clear();
                kategorije.add(srch.getText().toString());
                akategorije.addAll(kategorije);
                akategorije.notifyDataSetChanged();

                dkat.setEnabled(false);
                srch.setText("");
                akategorije.getFilter().filter(srch.getText().toString());

                }
        });

        dknjig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i1= new Intent(KategorijeAkt.this,DodavanjeKnjigeAkt.class);
                i1.putStringArrayListExtra("kat",kategorije);
                startActivity(i1);
            }
        });
još ova
        kat.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(KategorijeAkt.this, ListaKnjigaAkt.class);
                intent.putExtra("zanr", kategorije.get(i));
                startActivity(intent);
            }
        });*/
    }
}
