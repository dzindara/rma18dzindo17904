package ba.unsa.etf.rma.nedzad.spirala;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;

import static ba.unsa.etf.rma.nedzad.spirala.KategorijeAkt.baza;

/**
 * Created by Nedzad on 28.03.2018..
 */

public class Knjiga implements Parcelable{
    private  String id,ia,naziv,za,opis,datumObjavljivanja;
    private ArrayList<Autor> autori;
    private Bitmap slika1;
    private URL slika;
    private int obojen =0,brojStranica;

    public Knjiga (){}
    public Knjiga (String ia,String nk, String za,Bitmap slika){
        this.ia=ia;
        this.naziv=nk;
        this.za=za;
        this.slika1=slika;

        this.id="nema id";

        this.autori=new ArrayList<Autor>();

        this.opis="nema opisa";
        this.datumObjavljivanja="nema datuma";
      //  this.slika=slika;
        this.brojStranica=0;
    //    this.slika1=null;

        try {

            this.slika = new URL("https://");

        } catch (Exception e) {

        }
    }
    public Knjiga (String ia,String nk, String za){
        this.ia=ia;
        this.naziv=nk;
        this.za=za;
        this.slika1=null;
    }
    //spirala3
    public Knjiga(String id, String naziv,ArrayList<Autor> autori, String opis, String datumObjavljivanja, URL slika, int brojStranica){
        this.id=id;
        this.naziv=naziv;
        this.autori=new ArrayList<Autor>();
        this.autori=autori;
        this.opis=opis;
        this.datumObjavljivanja=datumObjavljivanja;
        this.slika=slika;
        this.brojStranica=brojStranica;
        this.slika1=null;
        this.ia="";
    }

    public Knjiga(String id, String naziv, ArrayList<Autor> autori, String opis, String datumobjave, URL slika, int brojStranica, int selektovan) {
        this.id=id;
        this.naziv=naziv;
        this.autori=new ArrayList<Autor>();
        this.autori=autori;
        this.opis=opis;
        this.datumObjavljivanja=datumObjavljivanja;
        this.slika=slika;
        this.brojStranica=brojStranica;
        this.slika1=null;
        this.ia="";
        this.obojen=selektovan;
    }


    public String ImeA(){return  ia;}
    public String NazivK(){return  naziv;}
    public String Zanr(){return  za;}
    public Bitmap Sliba(){return  slika1;}
    public void Selektuj (){ obojen=1; }
    public int Obojen (){return  obojen;}

    //spirala3
    //seteri
    public void setId(String id) {
        this.id = id;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public void setAutori(ArrayList<Autor> autori) {
        this.autori = autori;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public void setDatumObjavljivanja(String datumObjavljivanja) {
        this.datumObjavljivanja = datumObjavljivanja;
    }

    public void setSlika(URL slika) {
        this.slika = slika;
    }

    public void setBrojStranica(int brojStranica) {
        this.brojStranica = brojStranica;
    }

    public void setZa(String za) {
        this.za = za;
    }

    //geteri


    public Bitmap getSlika1() {
        return slika1;
    }

    public String getId() {
        return id;
    }

    public String getNaziv() {
        return naziv;
    }

    public ArrayList<Autor> getAutori() {
        return autori;
    }

    public String getOpis() {
        return opis;
    }

    public String getDatumObjavljivanja() {
        return datumObjavljivanja;
    }

    public URL getSlika() {
        return slika;
    }

    public int getBrojStranica() {
        return brojStranica;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(ia);
        parcel.writeString(naziv);
        parcel.writeString(za);
        parcel.writeString(opis);
        parcel.writeString(datumObjavljivanja);
        parcel.writeParcelable(slika1, i);
        parcel.writeInt(obojen);
        parcel.writeInt(brojStranica);
    }

    public Knjiga(Parcel pa){

        id = pa.readString();
        ia = pa.readString();
        naziv = pa.readString();
        za = pa.readString();
        opis = pa.readString();
        datumObjavljivanja = pa.readString();
        slika1 = pa.readParcelable(Bitmap.class.getClassLoader());
        obojen = pa.readInt();
        brojStranica = pa.readInt();
    }


    public static final Creator<Knjiga> CREATOR = new Creator<Knjiga>() {
        @Override
        public Knjiga createFromParcel(Parcel in) {
            return new Knjiga(in);
        }

        @Override
        public Knjiga[] newArray(int size) {
            return new Knjiga[size];
        }
    };
}
