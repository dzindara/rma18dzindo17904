package ba.unsa.etf.rma.nedzad.spirala;

/**
 * Created by Nedzad on 29.03.2018..
 */

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;



public class AdapterKnjige extends BaseAdapter {
    public ArrayList<Knjiga>  str= new ArrayList<Knjiga>();
    public LayoutInflater mojInflater;
    public Context ca;

    public AdapterKnjige(Context c, ArrayList<Knjiga> s){
        str=s;
        this.ca=c;
        mojInflater=(LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return str.size();
    }

    @Override
    public Object getItem(int i) {
        return str.get(i).ImeA();
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        View v = mojInflater.inflate(R.layout.element_liste, null);
        final TextView t1=(TextView) v.findViewById(R.id.eNaziv);
        final TextView t2=(TextView) v.findViewById(R.id.eAutor);
        ImageView im = (ImageView) v.findViewById(R.id.eNaslovna);
        Button b1= v.findViewById(R.id.dPreporuci);
        //@mipmap/ic_launcher
        try {
            if(str.get(i).getSlika1()==null && str.get(i).getSlika().equals(new URL( "https://"))){
                im.setImageResource(R.drawable.ic_launcher);



            }
            else if(str.get(i).getSlika1()!=null){
                im.setImageBitmap(str.get(i).getSlika1());
            }
            else {
                Picasso.get().load(str.get(i).getSlika().toString()).into(im);

            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        if(str.get(i).Obojen()==1){
            v.setBackgroundResource(R.color.selektovanaBoja);
        }

        if(str.get(i).ImeA().equals("")){
            if(str.get(i).getAutori().size()!=0){
                t2.setText(str.get(i).getAutori().get(0).getImeiPrezime());
            }

        }
        else t2.setText(str.get(i).ImeA());


        t1.setText(str.get(i).NazivK());

        TextView t3 = v.findViewById(R.id.eDatumObjavljivanja);
        TextView t4 = v.findViewById(R.id.eOpis);
        TextView t5 = v.findViewById(R.id.eBrojStranica);

        t3.setText(str.get(i).getDatumObjavljivanja());
        t4.setText(str.get(i).getOpis());
        t5.setText(Integer.toString(str.get(i).getBrojStranica()));


        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentPreporuci fp= new FragmentPreporuci();
                Bundle b = new Bundle();
                b.putString("naziv",t1.getText().toString());
                b.putString("autor",t2.getText().toString());
                fp.setArguments(b);
                ((FragmentActivity)ca).getFragmentManager().beginTransaction().replace(R.id.mjestoF,fp,fp.getTag()).addToBackStack(null).commit();
                }
        });


        return v;
    }
}