package ba.unsa.etf.rma.nedzad.spirala;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import static ba.unsa.etf.rma.nedzad.spirala.KategorijeAkt.baza;

/**
 * Created by Nedzad on 07.04.2018..
 */

public class ListeFragment extends Fragment/* implements  DohvatiKnjige.IDohvatiKnjigeDone,DohvatiNajnovije.IDohvatiNajnovijeDone*/ {
    static int prikazkategorije;
    static String zaprenijeti = "";
    ArrayAdapter<String> akategorije;


    private ArrayList<Knjiga> knjige1 = new ArrayList<Knjiga>();
    ListView lista;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.liste_fragment, container, false);
        lista = v.findViewById(R.id.listaKategorija);
        prikazkategorije = 1;
        Toast.makeText(getActivity(), "evo", Toast.LENGTH_LONG).show();
        final Button autori = v.findViewById(R.id.dAutori);
        Button kategorijed = v.findViewById(R.id.dKategorije);
        final Button pretraga = v.findViewById(R.id.dPretraga);
        final Button dodajkategoriju = v.findViewById(R.id.dDodajKategoriju);
        final Button dodajknjigu = v.findViewById(R.id.dDodajKnjigu);
        final EditText poljetekst = v.findViewById(R.id.tekstPretraga);
        dodajkategoriju.setEnabled(false);

        final Button onlineKnjige = v.findViewById(R.id.dDodajOnline);


        //     final ArrayAdapter<String>
        akategorije = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, ListaKnjiga.kategorije);
        final ArrayList<Autor> autoridd = new ArrayList<Autor>();


        //   akategorije.addAll(ListaKnjiga.kategorije);
        lista.setAdapter(akategorije);


        pretraga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //new DohvatiKnjige((DohvatiKnjige.IDohvatiKnjigeDone)ListeFragment.this).execute("house");
                //new DohvatiNajnovije((DohvatiNajnovije.IDohvatiNajnovijeDone)ListeFragment.this).execute("Ivo Andric");
                akategorije.getFilter().filter(poljetekst.getText().toString(), new Filter.FilterListener() {
                    @Override
                    public void onFilterComplete(int i) {
                        if (i == 0 && !poljetekst.getText().toString().isEmpty()) {
                            dodajkategoriju.setEnabled(true);
                        } else {
                            dodajkategoriju.setEnabled(false);
                        }
                    }
                });


            }
        });
        autori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prikazkategorije = 0;
                autoridd.clear();

                ListaKnjiga.autoriSvi=baza.dajSveAutore();



            /*    for (Knjiga k:ListaKnjiga.listak) {
                Boolean ima=false;
                for(Autor ia:autoridd){
                    if(k.ImeA().equals(ia.IA())){
                        ima=true; break;
                    }
                }
                if(!ima){int brknji=0;
                    for(Knjiga k1:ListaKnjiga.listak){if(k1.ImeA().equals(k.ImeA())){brknji++;}}
                    autoridd.add(new Autor(k.ImeA(),brknji));}
            }*/

                //    final AdapterAutor autoriii= new AdapterAutor(getActivity(),autoridd);
                //  lista.setAdapter(autoriii);

               final AdapterAutor autoriii = new AdapterAutor(getActivity(), ListaKnjiga.autoriSvi);
                lista.setAdapter(autoriii);

                pretraga.setVisibility(View.GONE);
                dodajkategoriju.setVisibility(View.GONE);
                poljetekst.setVisibility(View.GONE);
            }
        });


        dodajkategoriju.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                akategorije.clear();
                //ListaKnjiga.kategorije.add(poljetekst.getText().toString());
                baza.dodajKategoriju(poljetekst.getText().toString());
                ListaKnjiga.kategorije = baza.dajKategorije();
                akategorije.addAll(ListaKnjiga.kategorije);
                akategorije.notifyDataSetChanged();

                dodajkategoriju.setEnabled(false);
                poljetekst.setText("");
                akategorije.getFilter().filter(poljetekst.getText().toString());

            }
        });

        kategorijed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prikazkategorije = 1;
                //  final ArrayAdapter<String>
                akategorije = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, ListaKnjiga.kategorije);
                lista.setAdapter(akategorije);
                pretraga.setVisibility(View.VISIBLE);
                dodajkategoriju.setVisibility(View.VISIBLE);
                poljetekst.setVisibility(View.VISIBLE);

            }
        });

        onlineKnjige.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentOnline fo = new FragmentOnline();
                FragmentManager fm = getFragmentManager();
                fm.beginTransaction().replace(R.id.mjestoF, fo, fo.getTag()).addToBackStack(null).commit();
            }
        });


        dodajknjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DodavanjeKnjigeFragment dkf = new DodavanjeKnjigeFragment();
                FragmentManager fm = getFragmentManager();
                Bundle b = new Bundle();
                b.putStringArrayList("kat1", ListaKnjiga.kategorije);
                dkf.setArguments(b);
                fm.beginTransaction().replace(R.id.mjestoF, dkf, dkf.getTag()).addToBackStack(null).commit();
                if (KategorijeAkt.sili) {
                    KategorijeAkt.knjiged.setVisibility(View.GONE);
                }

            }
        });

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (prikazkategorije == 1) {

                    KnjigeFragment kf = new KnjigeFragment();
                    FragmentManager fm = getFragmentManager();
                    zaprenijeti = ListaKnjiga.kategorije.get(i);

                    if (KategorijeAkt.sili) {
                        fm.beginTransaction().replace(R.id.mjestoF2, kf, kf.getTag()).addToBackStack(null).commit();
                    } else {
                        fm.beginTransaction().replace(R.id.mjestoF, kf, kf.getTag()).addToBackStack(null).commit();
                    }
                } else {

                    KnjigeFragment kf = new KnjigeFragment();
                    FragmentManager fm = getFragmentManager();
                    zaprenijeti = ListaKnjiga.autoriSvi.get(i).IA();


                    if (KategorijeAkt.sili) {
                        fm.beginTransaction().replace(R.id.mjestoF2, kf, kf.getTag()).addToBackStack(null).commit();
                    } else {
                        fm.beginTransaction().replace(R.id.mjestoF, kf, kf.getTag()).addToBackStack(null).commit();
                    }

                }
            }
        });
        return v;
    }
/*
    @Override
    public void onDohvatiDone(ArrayList<Knjiga> knjige) {
        knjige1=knjige;
        Log.d("ecvo", knjige.get(1).getOpis());
        AdapterKnjige ak= new AdapterKnjige(getActivity(),knjige);
        lista.setAdapter(ak);

    }

    @Override
    public void onNajnovijeDone(ArrayList<Knjiga> knjige) {
        knjige1=knjige;
        Log.d("ecvo", knjige.get(1).getOpis());
        AdapterKnjige ak= new AdapterKnjige(getActivity(),knjige);
        lista.setAdapter(ak);
    }*/
}
