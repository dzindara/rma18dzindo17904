package ba.unsa.etf.rma.nedzad.spirala;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Nedzad on 07.04.2018..
 */

public class AdapterAutor extends BaseAdapter {
    public ArrayList<Autor> str= new ArrayList<Autor>();
    public LayoutInflater mojInflater;

    public AdapterAutor(Context c, ArrayList<Autor> s){
        str=s;
        mojInflater=(LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return str.size();
    }

    @Override
    public Object getItem(int i) {
        return str.get(i).IA();
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = mojInflater.inflate(R.layout.lista_autora, null);
        TextView t1=(TextView) v.findViewById(R.id.textView3);
        TextView t2=(TextView) v.findViewById(R.id.textView4);

     t2.setText((Integer.toString( str.get(i).Brk())));
       t1.setText(str.get(i).IA());


        return v;
    }

    public void addAll() {
    }
}
