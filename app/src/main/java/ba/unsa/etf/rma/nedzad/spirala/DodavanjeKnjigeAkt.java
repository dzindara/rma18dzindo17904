package ba.unsa.etf.rma.nedzad.spirala;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class DodavanjeKnjigeAkt extends AppCompatActivity {
        ImageView image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodavanje_knjige_akt);
        image =(ImageView) findViewById(R.id.naslovnaStr);

        final EditText imeAutora =findViewById(R.id.imeAutora);
        final EditText naslovKnjige =findViewById(R.id.nazivKnjige);
        Button dodajSliku = findViewById(R.id.dNadjiSliku);
        Button dodajKnjigu = findViewById(R.id.dUpisiKnjigu);
        Button povratak = findViewById(R.id.dPonisti);
        final Spinner zanr = findViewById(R.id.sKategorijaKnjige);

        Intent i = getIntent();
        ArrayList<String> kategorije=i.getStringArrayListExtra("kat");
        ArrayAdapter<String> kadapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,kategorije);
        zanr.setAdapter(kadapter);

        povratak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        dodajKnjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                image.buildDrawingCache();
                Knjiga tmp= new Knjiga(imeAutora.getText().toString(),naslovKnjige.getText().toString(),zanr.getSelectedItem().toString(),Bitmap.createBitmap(image.getDrawingCache()));
                ListaKnjiga.listak.add(tmp);
                Toast.makeText(getApplicationContext(),"Dodana je nova knjiga u zanru "+ zanr.getSelectedItem().toString()+" br: "+ ListaKnjiga.listak.size(),Toast.LENGTH_SHORT).show();

            }
        });
        dodajSliku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setType("image/*");
                i.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(i,""),2);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode,resultCode,data);
            if(requestCode==2 && resultCode==Activity.RESULT_OK){
                Uri uri1=data.getData();
                image.setImageURI(uri1);
            }

    }
}
