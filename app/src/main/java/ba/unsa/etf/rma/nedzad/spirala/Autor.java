package ba.unsa.etf.rma.nedzad.spirala;

import java.util.ArrayList;

/**
 * Created by Nedzad on 07.04.2018..
 */

public class Autor {
    private String imeiPrezime;
    private int brk;
    private ArrayList<String> knjige;

    Autor(){}
    Autor(String iip1, int brk1){
        imeiPrezime=iip1;
        brk=brk1;
        knjige=new ArrayList<String>();
    }

    //spirala3

    Autor(String iip, String id){
        this.imeiPrezime=iip;
        knjige=new ArrayList<String>();
        knjige.add(id);
    }

    public void dodajKnjigu(String id){
        boolean ima=false;
        for (String id1:knjige) {
            if(id1.equals(id)){
                ima=true; break;
            }
        }
        if(!ima){
            knjige.add(id);
        }
    }

    public String IA(){return imeiPrezime;}

    public int Brk(){return brk;}

    //spirala3 get/set


    public String getImeiPrezime() {
        return imeiPrezime;
    }

    public ArrayList<String> getKnjige() {
        return knjige;
    }

    public void setImeiPrezime(String imeiPrezime) {
        this.imeiPrezime = imeiPrezime;
    }

    public void setKnjige(ArrayList<String> knjige) {
        this.knjige = knjige;
    }

    public void setBrk() {
        this.brk++;
    }
}
