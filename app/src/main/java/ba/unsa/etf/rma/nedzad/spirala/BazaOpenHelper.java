package ba.unsa.etf.rma.nedzad.spirala;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Nedzad on 29.05.2018..
 */

public class BazaOpenHelper extends SQLiteOpenHelper {
    //osnovno
    public static final String DATABASE_NAME="mojaBazaSpirala.db";
    public static final int DATABASE_VERSION=5;

    //KATEGORIJA
    public static final String DATABASE_TABELA_KATEGORIJA="Kategorija";
    public static final String KATEGORIJA_ID="_id";
    public static final String KATEGORIJA_NAZIV="naziv";
    public static final String KREIRAJ_KATEGORIJU = "create table "+ DATABASE_TABELA_KATEGORIJA + " ("+ KATEGORIJA_ID +" integer primary key autoincrement, "+ KATEGORIJA_NAZIV + " text not null);";

    //KNJIGA
    public  static final String DATABASE_TABELA_KNJIGA="Knjiga";
    public  static final String KNJIGA_ID="_id";
    public  static final String KNJIGA_NAZIV="naziv";
    public  static final String KNJIGA_OPIS="opis";
    public  static final String KNJIGA_DATUM_OBJAVLJIVANJA="datumObjavljivanja";
    public  static final String KNJIGA_BROJ_STRANICA="brojStranica";
    public  static final String KNJIGA_ID_WEB="idWebServis";
    public  static final String KNJIGA_ID_KATEGORIJE="idKategorije";
    public  static final String KNJIGA_SLIKA="slika";
    public  static final String KNJIGA_PREGLEDANA="pregladana";
    public  static final String KREIRAJ_KNJIGU = "create table "+ DATABASE_TABELA_KNJIGA + " ("+ KNJIGA_ID + " integer primary key autoincrement, "+
            KNJIGA_NAZIV+ " text not null, "+ KNJIGA_OPIS+ " text, " +KNJIGA_DATUM_OBJAVLJIVANJA +" text, "+KNJIGA_BROJ_STRANICA+" integer , "+KNJIGA_ID_WEB+" text, "+
            KNJIGA_ID_KATEGORIJE + " integer," + KNJIGA_SLIKA+" text, "+ KNJIGA_PREGLEDANA + " integer);";

    //AUTOR
    public static final String DATABASE_TABELA_AUTOR="Autor";
    public static final String AUTOR_ID="_id";
    public static final String AUTOR_IME="ime";
    public static final String KREIRAJ_AUTORA="create table "+DATABASE_TABELA_AUTOR+" ("+AUTOR_ID+" integer primary key autoincrement, "+AUTOR_IME+ " text );";

    //AUTORSTVO

    public static final String DATABASE_TABELA_AUTORSTVO="Autorstvo";
    public static final String AUTORSTVO_ID="_id";
    public static final String AUTORSTVO_ID_AUTORA="idautora";
    public static final String AUTORSTVO_ID_KNJIGE="idknjige";
    public static final String KREIRAJ_AUTORSTVO="create table "+DATABASE_TABELA_AUTORSTVO+" ("+AUTORSTVO_ID+" integer primary key autoincrement, "+AUTORSTVO_ID_AUTORA+ " integer, "+ AUTORSTVO_ID_KNJIGE + " integer);";




    public BazaOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(KREIRAJ_KATEGORIJU);
        sqLiteDatabase.execSQL(KREIRAJ_KNJIGU);
        sqLiteDatabase.execSQL(KREIRAJ_AUTORA);
        sqLiteDatabase.execSQL(KREIRAJ_AUTORSTVO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+DATABASE_TABELA_KATEGORIJA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+DATABASE_TABELA_KNJIGA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+DATABASE_TABELA_AUTOR);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+DATABASE_TABELA_AUTORSTVO);
        onCreate(sqLiteDatabase);
    }

    public long dodajKategoriju(String naziv) {
        SQLiteDatabase baza = this.getReadableDatabase();
        String selectUpit = "SELECT * FROM " + DATABASE_TABELA_KATEGORIJA + " WHERE naziv = "+android.database.DatabaseUtils.sqlEscapeString(naziv)+";";
        Cursor c = baza.rawQuery(selectUpit, null);
        Boolean ima = false;
        if (c.getCount() != 0) {
            ima = true;
        }
        c.close();
        long rezultat;
        rezultat = 0;
        if (!ima) {
            baza = this.getWritableDatabase();
            ContentValues kategorija = new ContentValues();
            kategorija.put(KATEGORIJA_NAZIV, naziv);
            rezultat = baza.insert(DATABASE_TABELA_KATEGORIJA, null, kategorija);
            Log.e("NOVA", "DODANA");
            Log.d("_______________", String.valueOf(rezultat));


        }
        return rezultat;


    }

    public ArrayList<String> dajKategorije() {
        SQLiteDatabase baza = this.getReadableDatabase();
        String selectUpite= "SELECT * FROM " + DATABASE_TABELA_KATEGORIJA + ";";
        Cursor c = baza.rawQuery(selectUpite,null);
        ArrayList<String> kategorije=new ArrayList<String>();
        while (c.moveToNext()){
            String naziv = c.getString(c.getColumnIndex(KATEGORIJA_NAZIV));
            kategorije.add(naziv);
        }
        c.close();
        return kategorije;
    }

    //b) unikatno za onfline i online knjige je naziv tako da ako knjiga sa istim nazivom postoji ona se neće dodati

    long dodajKnjigu (Knjiga knjiga){
        SQLiteDatabase baza = this.getReadableDatabase();
        String selectUpit="SELECT * FROM "+ DATABASE_TABELA_KNJIGA + " WHERE "+KNJIGA_NAZIV+ " ="+android.database.DatabaseUtils.sqlEscapeString(knjiga.getNaziv())+";";
        Cursor c = baza.rawQuery(selectUpit,null);
        Boolean ima = false;
        if (c.getCount() != 0) {
            ima = true;
        }
        c.close();
        long rezultat;
        rezultat = 0;
        if (!ima) {

            //id kat
            baza=this.getReadableDatabase();
            selectUpit ="SELECT * FROM "+ DATABASE_TABELA_KATEGORIJA + " WHERE "+ KATEGORIJA_NAZIV + " ="+android.database.DatabaseUtils.sqlEscapeString(knjiga.Zanr())+";";
            c=baza.rawQuery(selectUpit,null);
            c.moveToNext();
            int id=c.getInt(c.getColumnIndex(KATEGORIJA_ID));
            c.close();

            //dod knjig


            baza = this.getWritableDatabase();
            ContentValues knjigazb = new ContentValues();
            knjigazb.put(KNJIGA_NAZIV,knjiga.getNaziv());
            knjigazb.put(KNJIGA_OPIS,knjiga.getOpis());
            knjigazb.put(KNJIGA_DATUM_OBJAVLJIVANJA,knjiga.getDatumObjavljivanja());
            knjigazb.put(KNJIGA_BROJ_STRANICA,knjiga.getBrojStranica());
            knjigazb.put(KNJIGA_ID_WEB,knjiga.getId());
            knjigazb.put(KNJIGA_ID_KATEGORIJE,id);
            knjigazb.put(KNJIGA_SLIKA,knjiga.getSlika().toString());
            knjigazb.put(KNJIGA_PREGLEDANA,knjiga.Obojen());
            rezultat = baza.insert(DATABASE_TABELA_KNJIGA, null, knjigazb);
            Log.e("NOVA", "DODANA");
            Log.d("_______________", String.valueOf(rezultat));




            //dod autora
            if(!knjiga.ImeA().equals("")){

                 baza = this.getReadableDatabase();
                 selectUpit="SELECT * FROM "+ DATABASE_TABELA_AUTOR + " WHERE "+AUTOR_IME+ " ="+android.database.DatabaseUtils.sqlEscapeString(knjiga.ImeA())+";";
                 c = baza.rawQuery(selectUpit,null);
                Boolean imaA = false;
                long rezultat2=0;
                if (c.getCount() != 0) {
                    imaA = true;
                    c.moveToNext();
                    rezultat2=c.getLong(c.getColumnIndex(AUTOR_ID));
                }
                c.close();


                if(!imaA){
                    baza=this.getWritableDatabase();
                    ContentValues autor = new ContentValues();
                    autor.put(AUTOR_IME,knjiga.ImeA());
                    rezultat2 = baza.insert(DATABASE_TABELA_AUTOR,null,autor);

                }
                else {
                    Log.d("JARUGO", "IMA AUTORA");
                }

                //upis autorstva;

                baza=this.getWritableDatabase();
                ContentValues autorstvo = new ContentValues();
                autorstvo.put(AUTORSTVO_ID_AUTORA,rezultat2);
                autorstvo.put(AUTORSTVO_ID_KNJIGE,rezultat);
                rezultat2 = baza.insert(DATABASE_TABELA_AUTORSTVO,null,autorstvo);

                 }
            else {
                for(Autor a: knjiga.getAutori()){
                    String imeautora= a.getImeiPrezime();

                    baza = this.getReadableDatabase();
                    selectUpit="SELECT * FROM "+ DATABASE_TABELA_AUTOR + " WHERE "+AUTOR_IME+ " ="+android.database.DatabaseUtils.sqlEscapeString(imeautora)+";";
                    c = baza.rawQuery(selectUpit,null);
                    Boolean imaA = false;
                    long rezultat2=0;
                    if (c.getCount() != 0) {
                        imaA = true;
                        c.moveToNext();
                        rezultat2=c.getLong(c.getColumnIndex(AUTOR_ID));
                    }
                    c.close();


                    if(!imaA){
                        baza=this.getWritableDatabase();
                        ContentValues autor = new ContentValues();
                        autor.put(AUTOR_IME,imeautora);
                        rezultat2 = baza.insert(DATABASE_TABELA_AUTOR,null,autor);

                    }
                    else {
                        Log.d("JARUGO", "IMA AUTORA");
                    }

                    //upis autorstva;

                    baza=this.getWritableDatabase();
                    ContentValues autorstvo = new ContentValues();
                    autorstvo.put(AUTORSTVO_ID_AUTORA,rezultat2);
                    autorstvo.put(AUTORSTVO_ID_KNJIGE,rezultat);
                    rezultat2 = baza.insert(DATABASE_TABELA_AUTORSTVO,null,autorstvo);

                }
            }
            //dod autorstva
        }
        return rezultat;


    }

    ArrayList<Knjiga> knjigeKategorije(long idKategorije) throws MalformedURLException {
        ArrayList<Knjiga> knjige=new ArrayList<Knjiga>();
        SQLiteDatabase baza=this.getReadableDatabase();
        String selectUpit="SELECT * FROM "+ DATABASE_TABELA_KNJIGA+" WHERE "+KNJIGA_ID_KATEGORIJE+" ="+Long.toString(idKategorije)+";";
        Cursor c=baza.rawQuery(selectUpit,null);
        while (c.moveToNext()){
            String id=c.getString(c.getColumnIndex(KNJIGA_ID_WEB));
            String naziv=c.getString(c.getColumnIndex(KNJIGA_NAZIV));
            String opis=c.getString(c.getColumnIndex(KNJIGA_OPIS));
            String datumobjave=c.getString(c.getColumnIndex(KNJIGA_DATUM_OBJAVLJIVANJA));
            URL slika;
            try {
                slika= new URL(c.getString(c.getColumnIndex(KNJIGA_SLIKA)));
            } catch (Exception e) {

                slika=new URL("https://");

            }
            int brojStranica=c.getInt(c.getColumnIndex(KNJIGA_BROJ_STRANICA));

            int idknjige=c.getInt(c.getColumnIndex(KNJIGA_ID));
            int selektovan=c.getInt(c.getColumnIndex(KNJIGA_PREGLEDANA));
            //id autora

            baza=this.getReadableDatabase();
            ArrayList<Integer> idAutora= new ArrayList<Integer>();
            selectUpit = "SELECT * FROM "+ DATABASE_TABELA_AUTORSTVO + " WHERE "+AUTORSTVO_ID_KNJIGE+ " ="+Integer.toString(idknjige)+";" ;
            Cursor c2=baza.rawQuery(selectUpit,null);
            Log.e("vel",Integer.toString(c2.getCount()));
            if(c2.getCount()==0){
                idAutora.add(1);
            }
            else{
                while (c2.moveToNext()){
                    Log.e("ID",Integer.toString(c2.getInt(c2.getColumnIndex(AUTORSTVO_ID_AUTORA))));
                    idAutora.add(c2.getInt(c2.getColumnIndex(AUTORSTVO_ID_AUTORA)));
                }

            }
            c2.close();
            ArrayList<Autor> autori=new ArrayList<Autor>();
            for(Integer i: idAutora){
                baza=this.getReadableDatabase();
                selectUpit ="SELECT * FROM "+DATABASE_TABELA_AUTOR+" WHERE "+AUTOR_ID+" ="+Integer.toString(i)+";";
                c2=baza.rawQuery(selectUpit,null);
                Log.e("EVO",Integer.toString(c2.getCount()));

                Log.e("EVO",Integer.toString(i));
                while (c2.moveToNext()){
                    String imeAutora=c2.getString(c2.getColumnIndex(AUTOR_IME));
                    autori.add(new Autor(imeAutora,id));
                }
                c2.close();
            }
            knjige.add(new Knjiga(id,naziv,autori,opis,datumobjave,slika,brojStranica,selektovan));


        }


        return knjige;

    }

    Integer dajIdKategorije(String naziv){
        SQLiteDatabase baza=this.getReadableDatabase();
        String selectU = "SELECT * FROM "+DATABASE_TABELA_KATEGORIJA + " WHERE "+KATEGORIJA_NAZIV + " ="+android.database.DatabaseUtils.sqlEscapeString(naziv)+";";
        Cursor c=baza.rawQuery(selectU,null);
        c.moveToNext();
        Integer id=c.getInt(c.getColumnIndex(KATEGORIJA_ID));
        c.close();
        return id;

    }

    public ArrayList<Autor> dajSveAutore() {
        ArrayList<Autor> autori=new ArrayList<Autor>();
        SQLiteDatabase baza= this.getReadableDatabase();
        String selectU="SELECT * FROM "+DATABASE_TABELA_AUTOR;
        Cursor c=baza.rawQuery(selectU,null);
        if(c.getCount()!=0){
            while (c.moveToNext()){
                String ime=c.getString(c.getColumnIndex(AUTOR_IME));
                Integer id=c.getInt(c.getColumnIndex(AUTOR_ID));
                selectU="SELECT * FROM "+DATABASE_TABELA_AUTORSTVO + " WHERE "+AUTORSTVO_ID_AUTORA+ " ="+id+";";
                Cursor c2=baza.rawQuery(selectU,null);
                Integer brojKnjiga=c2.getCount();
                autori.add(new Autor(ime,brojKnjiga));
            }
        }
        return autori;
    }

    public Integer dajIdAutora(String naziv) {
        SQLiteDatabase baza=this.getReadableDatabase();
        String selectU = "SELECT * FROM "+DATABASE_TABELA_AUTOR + " WHERE "+AUTOR_IME + "="+android.database.DatabaseUtils.sqlEscapeString(naziv)+";";
        Cursor c=baza.rawQuery(selectU,null);
        c.moveToNext();
        Integer id=c.getInt(c.getColumnIndex(KATEGORIJA_ID));
        c.close();
        return id;


    }

    public ArrayList<Knjiga> knjigeAutora(Integer id) throws MalformedURLException {
        ArrayList<Knjiga> knjige= new ArrayList<Knjiga>();
        SQLiteDatabase baza=this.getReadableDatabase();
        ArrayList<Integer> idoviKnjiga= new ArrayList<Integer>();
        String selectUpit="SELECT * FROM "+DATABASE_TABELA_AUTORSTVO+ " WHERE "+AUTORSTVO_ID_AUTORA+ " ="+Integer.toString(id)+";";
        Cursor c=baza.rawQuery(selectUpit,null);
        while(c.moveToNext()){
            idoviKnjiga.add(c.getInt(c.getColumnIndex(AUTORSTVO_ID_KNJIGE)));
        }
        c.close();
        for(Integer i:idoviKnjiga){
             selectUpit="SELECT * FROM "+ DATABASE_TABELA_KNJIGA+" WHERE "+KNJIGA_ID+" ="+Integer.toString(i)+";";
             c=baza.rawQuery(selectUpit,null);
            while (c.moveToNext()){
                String idw=c.getString(c.getColumnIndex(KNJIGA_ID_WEB));
                String naziv=c.getString(c.getColumnIndex(KNJIGA_NAZIV));
                String opis=c.getString(c.getColumnIndex(KNJIGA_OPIS));
                String datumobjave=c.getString(c.getColumnIndex(KNJIGA_DATUM_OBJAVLJIVANJA));
                URL slika;
                try {
                    slika= new URL(c.getString(c.getColumnIndex(KNJIGA_SLIKA)));
                } catch (Exception e) {

                    slika=new URL("https://");

                }
                int brojStranica=c.getInt(c.getColumnIndex(KNJIGA_BROJ_STRANICA));

                int idknjige=c.getInt(c.getColumnIndex(KNJIGA_ID));
                int selektovan=c.getInt(c.getColumnIndex(KNJIGA_PREGLEDANA));
                //id autora

                baza=this.getReadableDatabase();
                ArrayList<Integer> idAutora= new ArrayList<Integer>();
                selectUpit = "SELECT * FROM "+ DATABASE_TABELA_AUTORSTVO + " WHERE "+AUTORSTVO_ID_KNJIGE+ " ="+Integer.toString(idknjige)+";" ;
                Cursor c2=baza.rawQuery(selectUpit,null);
                Log.e("vel",Integer.toString(c2.getCount()));
                if(c2.getCount()==0){
                    idAutora.add(1);
                }
                else{
                    while (c2.moveToNext()){
                        Log.e("ID",Integer.toString(c2.getInt(c2.getColumnIndex(AUTORSTVO_ID_AUTORA))));
                        idAutora.add(c2.getInt(c2.getColumnIndex(AUTORSTVO_ID_AUTORA)));
                    }

                }
                c2.close();
                ArrayList<Autor> autori=new ArrayList<Autor>();
                for(Integer ii: idAutora){
                    baza=this.getReadableDatabase();
                    selectUpit ="SELECT * FROM "+DATABASE_TABELA_AUTOR+" WHERE "+AUTOR_ID+" ="+Integer.toString(ii)+";";
                    c2=baza.rawQuery(selectUpit,null);
                    Log.e("EVO",Integer.toString(c2.getCount()));

                    Log.e("EVO",Integer.toString(i));
                    while (c2.moveToNext()){
                        String imeAutora=c2.getString(c2.getColumnIndex(AUTOR_IME));
                        autori.add(new Autor(imeAutora,idw));
                    }
                    c2.close();
                }
                knjige.add(new Knjiga(idw,naziv,autori,opis,datumobjave,slika,brojStranica,selektovan));


            }

        }




        return knjige;
    }

    public void Oboji(String naziv){
        Log.d("NAZIV ",naziv);
        SQLiteDatabase baza=this.getWritableDatabase();
        ContentValues boja= new ContentValues();
        boja.put(KNJIGA_PREGLEDANA,1);
        baza.update(DATABASE_TABELA_KNJIGA,boja, KNJIGA_NAZIV+" ="+android.database.DatabaseUtils.sqlEscapeString(naziv)+"",null);


        baza=this.getReadableDatabase();
        String selex="SELECT * FROM " + DATABASE_TABELA_KNJIGA +" WHERE "+ KNJIGA_NAZIV + " ="+android.database.DatabaseUtils.sqlEscapeString(naziv)+";";
        Cursor c=baza.rawQuery(selex,null);
        c.moveToNext();
        Log.d("_BOJA_",Integer.toString(c.getInt(c.getColumnIndex(KNJIGA_PREGLEDANA)))+ " "+ naziv);
    }
}
