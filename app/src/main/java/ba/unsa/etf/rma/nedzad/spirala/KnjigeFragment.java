package ba.unsa.etf.rma.nedzad.spirala;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.net.MalformedURLException;
import java.util.ArrayList;

import static ba.unsa.etf.rma.nedzad.spirala.KategorijeAkt.baza;

/**
 * Created by Nedzad on 08.04.2018..
 */

public class KnjigeFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.knjige_fragment,container,false);

        final ListView listak = v.findViewById(R.id.listaKnjiga);
        ArrayList<Knjiga> kategoricno= new ArrayList<Knjiga>();
        if(ListeFragment.zaprenijeti.equals("")){ for(Knjiga k: ListaKnjiga.listak){
            if(k.ImeA().equals(ListeFragment.zaprenijeti)){
                kategoricno.add(k);
            }
        }}
        else{
            if(ListeFragment.prikazkategorije==1){
           /* for(Knjiga k: ListaKnjiga.listak){
                if(k.Zanr().equals(ListeFragment.zaprenijeti)){
                    kategoricno.add(k);
                }
            }*/

                Integer id=baza.dajIdKategorije(ListeFragment.zaprenijeti);
                Log.d("RADI","_____id______");
                try {
                    kategoricno=baza.knjigeKategorije(id);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }else {
          /* for(Knjiga k: ListaKnjiga.listak){
                if(k.ImeA().equals(ListeFragment.zaprenijeti)){
                    kategoricno.add(k);
                }
                else if(k.ImeA().equals("")){
                    for(Autor a:k.getAutori()){
                        if(a.getImeiPrezime().equals(ListeFragment.zaprenijeti)){
                            kategoricno.add(k);
                        }
                    }
                }
            }*/
                Integer id=baza.dajIdAutora(ListeFragment.zaprenijeti);
                Log.d("RADI","________id________");
                try {
                    kategoricno=baza.knjigeAutora(id);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        }


        final AdapterKnjige adapterknjige = new AdapterKnjige(getActivity(),kategoricno);
        listak.setAdapter(adapterknjige);
        Button povratak = v.findViewById(R.id.dPovratak);

        povratak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();
            }
        });

        final ArrayList<Knjiga> finalKategoricno = kategoricno;
        listak.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                view.setBackgroundResource(R.color.selektovanaBoja);
               baza.Oboji(finalKategoricno.get(i).getNaziv());
               /*
               Log.d("_BOJA2_",":");
                for(Knjiga k : ListaKnjiga.listak){
                    if(finalKategoricno.get(i).NazivK().equals(k.NazivK())){
                        k.Selektuj();
                        Log.d("_BOJA_",":");
                    }
                }
                */
            }
        });
        return v;
    }
}
