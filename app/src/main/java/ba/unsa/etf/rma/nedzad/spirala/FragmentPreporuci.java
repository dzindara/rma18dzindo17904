package ba.unsa.etf.rma.nedzad.spirala;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.IntentService;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Nedzad on 27.05.2018..
 */

public class FragmentPreporuci extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.preporuci_fragment,container,false);

        ArrayList<String> imena= new ArrayList<String>();
        final ArrayList<String> emailovi= new ArrayList<String>();

        //PODACI
        ContentResolver cr = getActivity().getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));


                //Cursor eC = getActivity().getApplicationContext().getContentResolver().query()

                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Email.CONTACT_ID +" = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {
                    //     String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        String email = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                      //  Log.d("_____________-",phoneNo +"___________"+ email);

                        if(!name.equals(null) && email.contains("@")){
                                        imena.add(name);
                                        emailovi.add(email);
                                            }
                       // Toast.makeText(getActivity(), "Name: " + name + ", Phone No: " + phoneNo, Toast.LENGTH_SHORT).show();
                    }
                    pCur.close();

            }
        }


        final Spinner sp1 = v.findViewById(R.id.sKontakti);



        ArrayAdapter<String> zaImena= new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_dropdown_item,imena);
        sp1.setAdapter(zaImena);
















        Button b1=v.findViewById(R.id.button323);
        TextView t1=v.findViewById(R.id.tPN);
        TextView t2=v.findViewById(R.id.tPA);
        Button b2=v.findViewById(R.id.dPosalji);


        final String s1=getArguments().getString("naziv");
        final String s2=getArguments().getString("autor");

        t1.setText(s1);
        t2.setText(s2);

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");

                String[] to={emailovi.get(sp1.getSelectedItemPosition())};
                String[] cc={""};


                emailIntent.putExtra(Intent.EXTRA_EMAIL,to);
                emailIntent.putExtra(Intent.EXTRA_CC,cc);
                emailIntent.putExtra(Intent.EXTRA_SUBJECT,"Spirala 4");
                emailIntent.putExtra(Intent.EXTRA_TEXT,"Zdravo "+sp1.getSelectedItem()+"\nPročitaj knjigu "+s1+" od "+s2+"!" );

                try{
                    startActivity(Intent.createChooser(emailIntent,"Pošalji email..."));

                }catch (android.content.ActivityNotFoundException e){
                    Log.d("EVO","NEMA EMAILA!");
                }
            }
        });
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();
            }
        });


        return v;
    }


    private void displayContacts(ArrayList<String> klix, ArrayList<String> emailovi) {


    }
}
